package com.PavelFofanov;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class EmitLog {
    private final static String EXCHANGE_NAME = "logs";
    private final static String HOST = "localhost";

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(HOST);

        try(Connection connection = connectionFactory.newConnection();
            Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

            String messege = args.length < 1 ? "Hello World" :
                    String.join(" ", args);
            channel.basicPublish(EXCHANGE_NAME, "", null, messege.getBytes(StandardCharsets.UTF_8));
            System.out.println("Send: " + messege);
        } catch (IOException | TimeoutException e) {

            throw new RuntimeException(e);
        }
    }
}
